#include <stdio.h>

// Default puzzle
int p[9][9] = {
6,8,0,1,0,2,3,0,0,
0,0,0,3,0,0,0,0,0,
0,0,2,4,8,0,7,1,0,
1,2,8,0,3,0,0,9,5,
0,0,4,0,0,0,1,0,0,
7,9,0,0,1,0,4,3,8,
0,4,9,0,7,3,5,0,0,
0,0,0,0,0,1,0,0,0,
0,0,3,5,0,8,0,6,7
};

int cells[9][9] = {};
int errorCheck = 0;

// Sort numbers into cells
int cellSort(void) {
  int counter = 0;
  int p1[81] = {};
  // Convert 2d puzzle to 1d
  for (int i = 0; i < 9; i++) {
    for (int d = 0; d < 9; d++) {
      p1[counter] = p[i][d];
      counter++;
    }
  }
  // Create a branching situation
  for (int i = 0; i < 9; i++) {
    switch (i) {
      case 0: counter = 0; break;
      case 1: counter = 3; break;
      case 2: counter = 6; break;
      case 3: counter = 27; break;
      case 4: counter = 30; break;
      case 5: counter = 33; break;
      case 6: counter = 54; break;
      case 7: counter = 57; break;
      case 8: counter = 60; break;
    }
    // Fill cells now
    cells[i][0] = p1[counter];
    cells[i][1] = p1[counter+1];
    cells[i][2] = p1[counter+2];
    cells[i][3] = p1[counter+9];
    cells[i][4] = p1[counter+10];
    cells[i][5] = p1[counter+11];
    cells[i][6] = p1[counter+18];
    cells[i][7] = p1[counter+19];
    cells[i][8] = p1[counter+20];
  }
  return 0;
}

// Count the zeros on the board
int zeroCounter(void) {
  int counter = 0;
  for (int i = 0; i < 9; i++) {
    for (int d = 0; d < 9; d++) {
      if (p[i][d] == 0) {
        // There's still a zero on the board
        counter += 1;
      }
    }
  }
  if (counter == 0) {
    // Puzzle complete!
    return 1;
  }
  if (errorCheck > 9) {
    printf("Puzzle too complex... sorry.\n");
    printf("This is all I\'ve got:\n");
    return 1;
  }
  // Still zeros
//  printf("Number of zeros left: %d\n",counter);
  return 0;
};

int searchNums(void) {
  for (int tNum = 1; tNum <= 9; tNum++) {
    // Locate all tNum on board
    // add to badZones for rule checking later
    int badZones[9][9] = {};
    // H Rows
    for (int i = 0; i < 9; i++) {
      for (int d = 0; d < 9; d++) {
        if (p[i][d]) {
          // If there's a num already
          // add to badZones
          badZones[i][d] = tNum;
        }
        if (tNum == p[i][d]) {
          // This row will be added to badZones
          for (int e = 0; e < 9; e++) {
            badZones[i][e] = tNum;
          }
        }
      }
    }
    // V Rows
    for (int i = 0; i < 9; i++) {
      for (int d = 0; d < 9; d++) {
        if (tNum == p[d][i]) {
        // This row will be added to badZones
          for (int e = 0; e < 9; e++) {
            badZones[e][i] = tNum;
          }
        }
      }
    }
    // Cells
    int badCells[9][9] = {};
    int revCells[9][9] = {};
    for (int i = 0; i < 9; i++) {
      for (int d = 0; d < 9; d++) {
        if (tNum == cells[i][d]) {
          for (int e = 0; e < 9; e++) {
            badCells[i][e] = tNum;
          }
        }
      }
    }
    // Revert badCells layout
    int counter = 0;
    int p1[81] = {};
    // Convert 2d puzzle to 1d
    for (int i = 0; i < 9; i++) {
      for (int d = 0; d < 9; d++) {
        p1[counter] = badCells[i][d];
        counter++;
      }
    }
    // Create a branching situation
    for (int i = 0; i < 9; i++) {
      switch (i) {
        case 0: counter = 0; break;
        case 1: counter = 3; break;
        case 2: counter = 6; break;
        case 3: counter = 27; break;
        case 4: counter = 30; break;
        case 5: counter = 33; break;
        case 6: counter = 54; break;
        case 7: counter = 57; break;
        case 8: counter = 60; break;
      }
      // Revert from cells now
      revCells[i][0] = p1[counter];
      revCells[i][1] = p1[counter+1];
      revCells[i][2] = p1[counter+2];
      revCells[i][3] = p1[counter+9];
      revCells[i][4] = p1[counter+10];
      revCells[i][5] = p1[counter+11];
      revCells[i][6] = p1[counter+18];
      revCells[i][7] = p1[counter+19];
      revCells[i][8] = p1[counter+20];
    }

    // Overlay revCells on badZones
    for (int i = 0; i < 9; i++) {
      for (int d = 0; d < 9; d++) {
        if (revCells[i][d] == tNum) {
          if (revCells[i][d] != badZones[i][d]) {
            badZones[i][d] = revCells[i][d];
          }
        }
      }
    }
    // First pass search for winners!
    // H Rows
    for (int i = 0; i < 9; i++) {
      int counter = 0;
      for (int d = 0; d < 9; d++) {
        if (badZones[i][d] == 0) {
          counter++;
        }
      }
      if (counter != 1) {
        // This ain't a row we're lookin' for
        for (int e = 0; e < 9; e++) {
          badZones[i][e] = tNum; // Fill with tNum
        }
      }
    }
    // V Rows
    for (int i = 0; i < 9; i++) {
      int counter = 0;
      for (int d = 0; d < 9; d++) {
        if (badZones[d][i] == 0) {
          counter++;
        }
      }
      if (counter != 1) {
        // This ain't a row we're lookin' for
        for (int e = 0; e < 9; e++) {
          badZones[e][i] = tNum; // Fill with tNum
        }
      }
    }
    for (int i = 0; i < 9; i++) {
      for (int d = 0; d < 9; d++) {
        if (badZones[i][d] == 0) {
          // We have a winner!
          p[i][d] = tNum;
          errorCheck = 0;
        }
      }
    }
    errorCheck++;
  }
  return 0;
};

int main(void) {
  while (zeroCounter() == 0) {
    cellSort();
    searchNums();
  }
  // Completed puzzle output:
  for (int i = 0; i < 9; i++) {
    for (int d = 0; d < 9; d++) {
      printf("%d",p[i][d]);
    } printf("\n");
  }
  return 0;
}
